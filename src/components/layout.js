import React from "react"
import "../styles/global.scss"
// import { css } from "@emotion/core"
import { useStaticQuery, Link, graphql } from "gatsby"
// import { rhythm } from  "../utils/typography"

// const LinkList = props => (
//     <li style={{ display: `inline-block`, marginRight: `1rem` }}>
//         <Link to={props.to}>{props.children}</Link>
//     </li>
// )

export default ({ children }) => {
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
      }
    `
  )

  return (
    <div className="m-5 p-2">
      
      <div className="mb-3">
      <Link to={`/`}>
        <h3 className="d-inline text-decoration-none">{data.site.siteMetadata.title}</h3>
      </Link>

      <Link className="float-right text-decoration-none" to={`/about/`}>
        About
      </Link>
      </div>

      <div className="shadow-lg p-3 mb-5 bg-white rounded">{children}</div>    
    </div>
  )
}
