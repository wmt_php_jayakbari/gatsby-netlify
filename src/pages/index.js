import React from "react"
import { Link, graphql } from "gatsby"
// import { css } from "@emotion/core"
// import { rhythm } from "../utils/typography"
import Layout from "../components/layout"

export default ({ data }) => {
    return (
      <Layout>
        <div>
        <h2 className="d-inline-block border-bottom">
          Amazing Pandas Eating Things
        </h2>
        <h5>{data.allMarkdownRemark.totalCount} Posts</h5>
        {data.allMarkdownRemark.edges.map(({node}) => (
          <div key={node.id}>
          <Link to={node.fields.slug} className="text-decoration-none text-secondary">
            <h4 className="mb-2">
              {node.frontmatter.title}{" "}
              <span className="text-black-50">
                — {node.frontmatter.date}
              </span>
            </h4>
            <p>{node.excerpt}</p>
            </Link>
          </div>
        ))}
        </div>
      </Layout>
    )
}

export const query = graphql `
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date
          }
          fields {
            slug
          }
          excerpt
        }
      }
    } 
  }
`