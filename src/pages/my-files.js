import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"

export default ({ data }) => {

    return (
        <Layout>
            <div>
                <h1>Site Demo</h1>
                <table>
                    <thead>
                        <tr>
                            <th>RelativePath</th>
                            <th>ID</th>
                            <th>PrettySize</th>
                            <th>Birthtime</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.allFile.edges.map(({ node }, index) => (
                            <tr key={index}>
                                <td>{node.relativePath}</td>
                                <td>{node.id}</td>
                                <td>{node.prettySize}</td>
                                <td>{node.birthtime}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </Layout>
    )
}

export const query = graphql `
query {
    allFile {
      edges {
        node {
          relativePath
          id
          prettySize
          birthtime
        }
      }
    }
  }
  `